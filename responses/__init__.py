"""
This code snippet defines a class called `Responses` that handles the initialization of a `Responses` object and returns a JSON response based on the provided data and exception.

Example Usage:
    response = Responses(data={'page': 'home'}, is_error=True, message='Error occurred')

Inputs:
    - data (optional): A dictionary containing the data to be included in the JSON response.
    - exception (optional): An exception object that occurred, if any.
    - **kwargs (optional): Additional keyword arguments that can be used to customize the response.

Flow:
1. The `data`, `exception`, and `**kwargs` are assigned to instance variables.
2. If an exception is provided, the `is_error` flag is set to `True` and the exception message is converted to a string and assigned to the `message` variable.
3. If no exception is provided, the `is_error` flag is determined based on the `is_error` keyword argument (defaulting to `False`) and the `message` is determined based on the `message` keyword argument (defaulting to an empty string).
4. If the `data` is not `None`, is an instance of a dictionary, and contains a key called `'page'`, the `is_error` flag and `message` are added to the `data` dictionary.
5. The modified `data` dictionary is returned as a JSON response using the `JsonResponse` class.
6. If the above condition is not met, the `Responses` object itself is returned as a JSON response using the `JsonResponse` class.

Outputs:
The code snippet returns a JSON response either with the modified `data` dictionary or the `Responses` object itself.
"""

# from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework.views import exception_handler
from rest_framework.serializers import ValidationError

def custom_exception_handler(exc, context):
    # Use the default exception handler to get the standard error response.
    response = exception_handler(exc, context)

    if response is not None:
        # Customize the error response format.
        data = response.data
        message = data.get('message', None)
        if message and isinstance(message, list):
            message = message if len(message) < 1 else message[0]
            data.pop('message')
        
        
        
        error_data = {
            'is_error': True,
            'message': message,
            'detail': data if isinstance(data, dict) else message
        }
        response.data = error_data

    return response

class Validation(ValidationError):
    def __init__(self, message=None, code=None, **kwargs):
        detail = {
            'message': message
        }
        detail.update(kwargs)
        super().__init__(detail, code)

class Responses(JsonResponse):
    def __init__(self, data=None, exception=None, **kwargs):
        """
        Initialize a Responses object.

        Args:
            data (dict, optional): A dictionary containing the data to be included in the JSON response.
            exception (Exception, optional): An exception object that occurred, if any.
            **kwargs (optional): Additional keyword arguments that can be used to customize the response.
        """
        # Automatically determine is_error and set message from the exception
        
        if exception is not None:
            is_error = True
            message = str(exception)
        else:
            is_error = kwargs.get('is_error', False)
            message = kwargs.get('message', '')
        
        if data is not None and isinstance(data, dict) and data.get('page'):
            data['is_error'] = is_error
            data['message'] = message
            super().__init__(data, status=400 if is_error else 200)
        else:
            response_data = {
                'data': data,
                'is_error': is_error,
                'message': message,
            }
            response_data.update(kwargs)
            super().__init__(response_data, status=400 if is_error else 200) 
        
        