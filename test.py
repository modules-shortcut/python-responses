import unittest
import json
from responses import Responses  # Import your Responses class from your module

class TestResponses(unittest.TestCase):
    def setUp(self):
        # Create an instance of Responses for testing
        self.response = Responses(
            code=200,
            is_error=False,
            message='Success',
            status=204,
            data={'key': 'value'}
        )

    def test_json_method(self):
        expected_json = {
            'code': 200,
            'is_error': False,
            'message': 'Success',
            'status': 204,
            'data': {'key': 'value'}
        }
        self.assertEqual(self.response.json(), expected_json)

    def test_to_json_method(self):
        expected_json_string = json.dumps({
            'code': 200,
            'is_error': False,
            'message': 'Success',
            'status': 204,
            'data': {'key': 'value'}
        })
        self.assertEqual(self.response.to_json(), expected_json_string)

    def test_str_method(self):
        expected_json_string = json.dumps({
            'code': 200,
            'is_error': False,
            'message': 'Success',
            'status': 204,
            'data': {'key': 'value'}
        })
        self.assertEqual(str(self.response), expected_json_string)

if __name__ == '__main__':
    unittest.main()
