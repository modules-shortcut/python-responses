from setuptools import setup, find_packages

setup(
    name='responses',
    version='1.1',
    author='dayat@rumahlogic.com',
    description='Universal response json',
    packages=find_packages(),
)